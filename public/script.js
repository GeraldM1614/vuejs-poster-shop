const LOAD_NUM = 10;
new Vue({

el: '#app',
data: {
  Price: 9,
  total: 0,
  items: [],
  cart: [],
  results:[],
  search: 'anime',
  lastSearch: '',
  loading: false
},
computed: {
  noMoreItems: function() {
    return this.items.length === this.results.length && this.results.length > 0
  }
},
methods: {
  
  appendItems: function() {
    if (this.items.length < this.results.length) {
      var append = this.results.slice(this.items.length, this.items.length + LOAD_NUM);
      this.items = this.items.concat(append);
    }
  },
  onSubmit: function () {
    if (this.search.length) {
      this.items=[];
      this.loading=true;
      axios.get('/search/'.concat(this.search))
      .then(response => {
        this.lastSearch= this.search;
        this.results =response.data;
        this.appendItems();
        this.loading=false;
      })
      .catch(error => {
        console.log(error);
      });
    }

  },
  addItem: function (index) {
    var item = this.items[index];
    this.total += this.Price;
    //var found = false;
    // for (var i = 0; i < this.cart.length; i++) {
    //   if (this.cart[i].id === item.id) {
    //     this.cart[i].qty++;
    //     found= true;
    //   }
    // }
    // var ind = this.cart.findIndex(element => {
    //   return element.id === item.id;
    // });
 
      var ind = this.cart.findIndex(element => {
        return element.id === item.id;
      });
      if (ind>=0) {
      this.cart[ind].qty++;
    } else {
      this.cart.push({
        id:item.id,
        title: item.title,
        qty: 1,
        price: this.Price
      });
    }
  },
  inc: function(item) {
    item.qty++;
    this.total+=item.price
  },
  dec: function(item) {
    item.qty--;
    this.total-=item.price;
    if (item.qty <=0) {
      var index = this.cart.findIndex(element => {
       return element.id === item.id;
     });
     this.cart.splice(index, 1)
      };
    }
},
filters: {
  currency: function(price) {
    return '$'.concat(price.toFixed(2));
  }
},
mounted: function() {
  this.onSubmit();

  var vueInstance = this;
  var elem = document.getElementById('product-list-bottom');
  var watcher = scrollMonitor.create(elem);
  watcher.enterViewport(function() {
  vueInstance.appendItems();
})

}
});

